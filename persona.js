
export class Persona {

  constructor(nombre_completo, id, edad){

    this.nombre_completo = nombre_completo,
    this.id = id;
    this.edad = edad;
  }
  
  getNombre_completo(){
   return this.nombre_completo;
  }

  getId(){
    return this.id;
  }

  getEdad(){
    return this.edad;
  }

  setNombre_completo(nombre_completo){
    this.nombre_completo = nombre_completo;
  }
  
}