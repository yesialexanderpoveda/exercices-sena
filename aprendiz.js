import {Persona} from "./persona.js" 

export class Aprendiz extends Persona{

    constructor(nombre_completo, id, edad, uniforme){
        super(nombre_completo, id, edad)
        this.uniforme = uniforme;    
    }

    getUniforme(){
        return this.uniforme
    }
    getAllData(){
        return `Aprendiz:  nombre: ${ this.nombre_completo}  id: ${this.id}  edad: ${this.edad} uniforme: ${this.uniforme}`
    }

}




